package com.atlassian.jira.plugins.useradmin;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;

import java.util.Collection;

/**
 *
 * @since v1.0
 */
@SuppressWarnings("unused")
public class UserAdmin extends JiraWebActionSupport {

    private final WebResourceManager webResourceManager;
    private final ProjectService projectService;
    private final JiraAuthenticationContext authContext;
    private final GroupManager groupManager;
    private final ProjectRoleManager projectRoleManager;

    private Collection<Project> projects;
    private Collection<Group> groups;
    private Collection<ProjectRole> roles;

    public UserAdmin(WebResourceManager webResourceManager, ProjectService projectService,
                     JiraAuthenticationContext authContext, GroupManager groupManager, ProjectRoleManager projectRoleManager) {
        this.webResourceManager = webResourceManager;
        this.projectService = projectService;
        this.authContext = authContext;
        this.groupManager = groupManager;
        this.projectRoleManager = projectRoleManager;
    }


    @Override
    public String doDefault() throws Exception {
        webResourceManager.requireResource("com.atlassian.jira.plugins.useradmin.jira-user-administration-plugin:js");
        projects = projectService.getAllProjects(authContext.getLoggedInUser()).getReturnedValue();
        groups = groupManager.getAllGroups();
        roles = projectRoleManager.getProjectRoles();
        return INPUT;
    }


    @Override
    protected String doExecute() throws Exception {
        return getRedirect("UserAdmin!default.jspa");
    }

    public Collection<Project> getProjects() {
        return projects;
    }

    public Collection<Group> getGroups() {
        return groups;
    }

    public Collection<ProjectRole> getRoles() {
        return roles;
    }
}
